### Relevant Articles:
- Import 2 maven project demo <br>
  test-projects\spring-security-sso\spring-security-sso-ui-2 <br>
  test-projects\spring-security-sso\spring-security-sso-ui <br>
- Chạy 2 project này để test local (tượng trưng cho 2 web app client): <br>
  http://localhost:8082/ <br>
  http://localhost:8083/ <br>
- 2 demo web app client đã deploy online <br>
  https://demo-hust-ui1.herokuapp.com/ <br>
  https://demo-hust-ui2.herokuapp.com/ <br>
- Link SSO <br>
  https://hust-sso.appspot.com/oauth/authorize?client_id=hust-client-xyz&redirect_uri=http://xyz.hust.edu.vn/&response_type=code&state=nedeL6 <br>
  client_id tương ứng với subdomain của hust.edu.vn<br>
