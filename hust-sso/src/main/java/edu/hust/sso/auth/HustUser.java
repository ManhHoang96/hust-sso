package edu.hust.sso.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HustUser {
    private String id;
    private String fullName;
    private String password;
    private long birthdate;
    private int gender;

    private int departmentId;
    private int programId;

    private String className;
    private String studentYear;
    private String phoneNumber;
    private String email;
}
