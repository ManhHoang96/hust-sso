package edu.hust.sso.auth;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HustUserResponse {
    private int status;
//    private HustUser data;
    private String data;
}
