package edu.hust.sso.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Base64;
import java.util.Collections;
import java.util.UUID;

@Component
public class HustAPIAuthenticationProvider implements AuthenticationProvider {
    private static byte[] getKey(String keyRule) {
        Key key = null;
        byte[] keyByte = keyRule.getBytes();
        // Create eight an empty array, the default is 0
        byte[] byteTemp = new byte[8];
        // The user specified rules into eight bit array
        for (int i = 0; i < byteTemp.length && i < keyByte.length; i++) {
            byteTemp[i] = keyByte[i];
        }
        key = new SecretKeySpec(byteTemp, "DES");
        return key.getEncoded();
    }

    private String authenticateHustUser(String username, String password) {
        String encryptedPassword = this.encryptPassword(username, password);
        String hustUser = null;
        try {
            val restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Type", "application/json;charset=UTF-8");
            HttpEntity<String> entity = new HttpEntity<>(headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    String.format(
                            "https://qldt.hust.edu.vn/api?action=login&birthday=-1&sessionid=%s&username=%s&password=%s",
                            UUID.randomUUID().toString(),
                            username,
                            encryptedPassword
                    )
                    , HttpMethod.POST, entity, String.class
            );

            val json = new ObjectMapper();
            val res = json.readValue(response.getBody(), HustUserResponse.class);
            hustUser = res == null ? null : res.getData();
        } catch (Exception e) {
            e.printStackTrace();
            hustUser = null;
        }

        if (hustUser == null) {
            try {
                val restTemplate = new RestTemplate();
                HttpHeaders headers = new HttpHeaders();
                headers.set("Content-Type", "application/json;charset=UTF-8");
                HttpEntity<String> entity = new HttpEntity<>(headers);

                ResponseEntity<String> response = restTemplate.exchange(
                        String.format(
                                "https://qldt.hust.edu.vn/api?action=login&birthday=-1&sessionid=%s&username=%s&password=%s",
                                UUID.randomUUID().toString(),
                                username,
                                password
                        )
                        , HttpMethod.POST, entity, String.class
                );

                val json = new ObjectMapper();
                val res = json.readValue(response.getBody(), HustUserResponse.class);
                hustUser = res == null ? null : res.getData();
            } catch (Exception e) {
                e.printStackTrace();
                hustUser = null;
            }
        }

        return hustUser;
    }

    private String encryptPassword(String username, String password) {
        try {
            if (!username.equals(password) && !password.equalsIgnoreCase("Hungnt@701B1")) {
                String keyMD5 = String.format("%s.%s", username, password);
                String key = DigestUtils.md5Hex(keyMD5);
                SecretKeySpec desKey = new SecretKeySpec(getKey(key), "DES");

                Cipher desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");

                desCipher.init(Cipher.ENCRYPT_MODE, desKey);
                byte[] encryptedPasswordBytes = desCipher.doFinal(password.getBytes());
                return Base64.getEncoder().encodeToString(encryptedPasswordBytes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return password;
    }

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        final String username = auth.getName();
        final String password = auth.getCredentials().toString();
        System.out.println("Login request");
        System.out.println(username + "    " + password);

        String hustUser = authenticateHustUser(username, password);
        if (hustUser == null) {
            System.out.println("Login failed");
            throw new BadCredentialsException("Tên đăng nhập hoặc mật khẩu không chính xác");
        } else {
            System.out.println("Login succeeded");
            try {
                String principal = hustUser;
                return new UsernamePasswordAuthenticationToken(principal, password, Collections.singletonList(new SimpleGrantedAuthority("USER")));
            } catch (Exception e) {
                throw new BadCredentialsException("Có lỗi xảy ra khi đăng nhập");
            }
        }
    }

    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }
}
