package edu.hust.sso;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter {
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public AuthServerConfig(BCryptPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()");
    }

    public ClientDetailsService clientDetailsService() {
        return new ClientDetailsService() {
            @Override
            public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
                BaseClientDetails details = new BaseClientDetails();
                details.setClientId(clientId);
                details.setClientSecret(passwordEncoder.encode("secret"));
                // Process based on clientId
                if (clientId.equalsIgnoreCase("demo-client")) {
                    details.setRegisteredRedirectUri(new HashSet<>(Arrays.asList(
                            "http://localhost:8082/login",
                            "http://localhost:8082/",
                            "http://localhost:8083//login",
                            "http://localhost:8083/",

                            "http://localhost:3000/login",
                            "http://localhost:3000/",
                            "http://localhost:3000/api/user/callback",
                            "http://localhost:3001/login",
                            "http://localhost:3001/",
                            "http://localhost:3001/api/user/callback",

                            "http://demo-ui1.hust.edu.vn:8082//login",
                            "http://demo-ui1.hust.edu.vn:8082/",
                            "http://demo-ui2.hust.edu.vn:8083/login",
                            "http://demo-ui2.hust.edu.vn:8083/",

                            "https://demo-hust-ui1.herokuapp.com/login",
                            "https://demo-hust-ui1.herokuapp.com/",
                            "https://demo-hust-ui2.herokuapp.com/login",
                            "https://demo-hust-ui2.herokuapp.com/"
                    )));
                } else if (clientId.startsWith("hust-client-")) {
                    String subDomain = StringUtils.removeStart(clientId, "hust-client-");
                    details.setRegisteredRedirectUri(new HashSet<>(Arrays.asList(
                            String.format("http://%s.hust.edu.vn/", subDomain),
                            String.format("http://%s.hust.edu.vn/login", subDomain),
                            String.format("http://%s.hust.edu.vn/callback", subDomain),
                            String.format("https://%s.hust.edu.vn/", subDomain),
                            String.format("https://%s.hust.edu.vn/login", subDomain),
                            String.format("https://%s.hust.edu.vn/callback", subDomain)
                    )));
                } else if (clientId.startsWith("online-learning-client")) {
                    details.setRegisteredRedirectUri(new HashSet<>(Arrays.asList(
                            "http://online-learning.hust.edu.vn:3001/user/"

                    )));
                } else throw new ClientRegistrationException("Invalid clientId");

                details.setAccessTokenValiditySeconds(3 * 60 * 60);
                details.setAuthorizedGrantTypes(Arrays.asList("authorization_code"));
                details.setScope(Arrays.asList("user_info"));
                details.setAutoApproveScopes(Arrays.asList("user_info"));
                Set<GrantedAuthority> authorities = new HashSet<>();
                authorities.add(new SimpleGrantedAuthority("USER"));
                details.setAuthorities(authorities);
                return details;
            }
        };
    }  //*/

    @Override
    public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {
        clients.withClientDetails(clientDetailsService());
    }
}
